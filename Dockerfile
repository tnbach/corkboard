FROM rust:1.76.0 AS builder
WORKDIR /usr/src/corkboard
COPY . .
RUN cargo build --release

# Runtime Stage
FROM ubuntu:22.04
COPY --from=builder /usr/src/corkboard/target/release/corkboard /usr/local/bin/corkboard
EXPOSE 5040
CMD ["corkboard"]
